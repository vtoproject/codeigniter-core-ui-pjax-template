<?php $this->load->view('template/head'); ?>
  <?php $this->load->view('template/header'); ?>
    <div class="app-body" id="pjax-container">
      <?php $this->load->view('template/left-menu'); ?>
        <main class="main" >            
          <?php $this->load->view('template/breadcrumb'); ?>
 
          <?php $this->load->view('content'); ?>          
          
        </main>
          <?php $this->load->view('template/right-menu'); ?>
    </div>
<?php $this->load->view('template/footer'); ?>
 